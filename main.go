package main

import (
	"fmt"
	"math/rand"
  "time"
)

func matrisRandomS(a int){
  mx := make([][]int, a)
  vx := make([][]int, a)
  for idx := range mx {
    mx[idx] = rand.Perm(a)
    vx[idx] = rand.Perm(a)
  }
  for ii := 0; ii<a; ii++ {
    for jj:=0; jj<a; jj++ {
      suma:=0
      for k:=0; k<a; k++ {
        suma += mx[ii][k] * vx[k][jj]
      }
    }
  }
}


var quit chan int = make(chan int)

func hilosM(mat []int,j int,i int){ 
  suma:=0
  for i:= 0; i<len(mat); i++ {
    suma=suma+mat[i]
  }
}

func matrisRandom(a int){
  mx := make([][]int, a)
  vx := make([][]int, a)
  for idx := range mx {
    mx[idx] = rand.Perm(a)
    vx[idx] = rand.Perm(a)
  }
  for i := 0; i<a; i++ {
    for j:=0; j<a; j++ {
      temp := make([]int, a)
      for k:=0; k<a; k++ {
        temp[k]= mx[i][k] * vx[k][j]
      }
      go hilosM(temp, j,i)
    }
  }
}


func main() {
  
  var start time.Time
  
  for i := 1; i <= 300; i++ {
    start = time.Now()
    /*matrisRandom(i)*/
    matrisRandomS(i)
    fmt.Println(time.Now().Sub(start).Seconds())
  }
}